package com.company;
/**
 * Класс День рождения со свойствами person, place, age
 * @author Автор
 * @version 1.0
 * @since 1.8
 */
public class Point {
    /**
     * . poltzn
     */
    private int x;
    /**
     * dont know
     */
    private Double y;
    /**
     * name pointa dla togo chtobi xoroso
     */
    public String name;
    /**
     * skoka nuzhno dic chtobi ono napicatalas
     */
    private Double dic;

    /**
     * soztat clas s x - 0, y - 0
     */
    public Point() {
        x = 0;
        y = 0.0;
    }

    /**
     * constr Point
     * @param x -  Parametr dla ploxig
     * @param y - ehe ploh
     */
    public Point(int x, Double y, String name, Double dic) {
        this.x = x;
        this.y = y;
        this.name = name;
        this.dic = dic;
    }

    /**
     * dla vivoda getint
     * @return int
     */
    public int getX() {
        return x;
    }

    /**
     * dla togo chtobi posmotret
     * @param x int
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * dla togo chtobi krasivo napixh
     * @return string
     */
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
