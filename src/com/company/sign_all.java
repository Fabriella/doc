package com.company;

import java.util.ArrayList;

public class sign_all {
    ArrayList<sign> signs = new ArrayList<>();

    public sign_all() {
    }
    public void addSing(sign s){
        signs.add(s);
    }

    public ArrayList<sign> getSigns() {
        return signs;
    }

    @Override
    public String toString() {
        String tabl = "<table bgcolor=\"#e0b46c\" align=\"center\" cellpadding=\"7\">" +
                "<tr bgcolor=\"#bd7f2a\">" +
                "<td>signature</td>" +
                "<td>Deprecateon</td>" +
                "<td>Param</td>" +
                "<td>Return</td>" +
                "</tr>";
        for (sign s: signs){
            tabl += s.toString();
        }
        tabl += "</table>";
        return tabl;
    }
}
